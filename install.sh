#!/bin/bash
sudo apt update
sudo apt upgrade

echo "Installing libraries for android"
#sudo apt install -y lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6
#sudo apt install -y lib64stdc++6:i386
#sudo apt install -y mesa-utils
#sudo apt install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386


#echo "Installing grub-customizer"
#sudo add-apt-repository -y ppa:danielrichter2007/grub-customizer
#sudo apt update
#sudo apt install -y grub-customizer

#echo "Installing clipit"
#sudo apt install -y clipit
#mkdir $HOME/.config/clipit
#cp clipitrc $HOME/.config/clipit/

echo "Installing geany"
sudo apt install -y geany
mkdir $HOME/.config/geany
cp geany.conf $HOME/.config/geany/
cp keybindings.conf $HOME/.config/geany/


echo "Installing google chrome"
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt update
sudo apt install -y google-chrome-stable


echo "Installing another tools"
sudo apt install -y gparted
sudo apt install -y vlc
sudo apt install -y gimp
sudo apt install -y git
sudo apt install -y gitk
sudo apt install -y curl
sudo apt install -y transmission
sudo apt install -y nmap
sudo apt install -y youtube-dl
sudo apt install -y copyq


echo "Installing Brightness Manager"
#sudo apt install -y gddccontrol
#sudo sh -c "echo '$USER ALL=(ALL) NOPASSWD: /usr/bin/ddccontrol' >> /etc/sudoers"
#sudo sh -c "echo '$USER ALL=(ALL) NOPASSWD: /usr/bin/gddccontrol' >> /etc/sudoers"


echo "Installing Linux Tools"
sudo apt install -y gnome-tweak-tool
sudo apt install -y chrome-gnome-shell
sudo apt install -y net-toos


echo "Installing Zoom"
sudo snap install zoom-client


echo "Installing Slack"
sudo snap install slack --classic


sudo snap install postman


echo "Installing mongodb"
#wget -qO - https://www.mongodb.org/static/pgp/server-4.0.asc | sudo apt-key add -
#echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
#sudo apt update
#sudo apt install -y mongodb-org
#sudo mkdir -p /data/db
#sudo chown -R mongodb /data
#sudo chmod -R 700 /data
#sudo systemctl enable mongod.service
#sudo service mongod start


echo "Configuring git; P4Merge and envioronment variables"
cp .gitconfig $HOME


echo "Loading gnome settings"
dconf load / <gnome.conf


echo "Installing Open VPN"
#sudo apt-get install -y --reinstall network-manager network-manager-gnome network-manager-openvpn network-manager-openvpn-gnome
#sudo service network-manager restart
